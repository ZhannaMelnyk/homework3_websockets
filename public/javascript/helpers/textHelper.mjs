export const getFinishedLetters = (letters, player) => {
  let finishedLetters = letters.slice(0, player.currentWordIndex);
  finishedLetters = finishedLetters.join('');
  return finishedLetters;
}

export const getCurrentLetter = (letters, player) => {
  return letters[player.currentWordIndex];
}

export const getNextLetters = (letters, player) => {
  let nextLetters = letters.slice(player.currentWordIndex + 1, letters.length);
  nextLetters = nextLetters.join('');
  return nextLetters;
}