import { createElement, removeClass, addClass } from './domHelper.mjs';
import { updateRoomsList, updatePlayersInGameRoom } from './updateHelper.mjs'

const lobbyPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');

export const toGameRoom = (room, socket) => {
  addClass(lobbyPage, 'display-none');
  removeClass(gamePage, 'display-none');

  const currentPlayer = room.players.find(player => player.socketID === socket.id);

  gamePage.innerHTML = "";

  const gameName = createElement({
    tagName: 'h2',
    className: 'game-name',
    innerText: room.name
  });

  const leaveRoomBtn = createElement({
    tagName: 'button',
    className: 'btn leave-room-btn',
    attributes: { id: 'leave-room-btn' },
    innerText: 'Back To Rooms'
  });

  leaveRoomBtn.addEventListener('click', () => {
    socket.emit('LEAVE_ROOM', room.name);
  });

  const playersContainer = updatePlayersInGameRoom(room.players, socket);

  const gameContainer = createElement({
    tagName: 'div',
    className: 'game-container',
    attributes: { id: `game-container` }
  });

  const isReadyBtn = createElement({
    tagName: 'button',
    className: 'btn isReady',
    attributes: { id: 'isReady' },
    innerText: 'Ready'
  });

  const playerIsReady = () => {
    socket.emit('PLAYER_IS_READY', room.name, currentPlayer.name);
    isReadyBtn.removeEventListener('click', playerIsReady)
  }

  isReadyBtn.addEventListener('click', playerIsReady);

  const timerBeforeGame = createElement({
    tagName: 'h2',
    className: 'time-before-game',
    attributes: { id: `time-before-game` }
  })

  timerBeforeGame.style.display = 'none';

  const timerForGame = createElement({
    tagName: 'span',
    className: 'time-for-game',
    attributes: { id: 'time-for-game' }
  })

  timerForGame.style.display = 'none';

  const gameTextBlock = createElement({
    tagName: 'div',
    className: 'game-text-block',
    attributes: { id: `game-text-block` }
  })

  gameTextBlock.style.display = 'none';

  gameContainer.append(isReadyBtn, timerBeforeGame, timerForGame, gameTextBlock);
  gamePage.append(gameName, leaveRoomBtn, playersContainer, gameContainer);
}

export const toLobby = (rooms, socket) => {
  addClass(gamePage, 'display-none');
  removeClass(lobbyPage, 'display-none');
  updateRoomsList(rooms, socket)
}