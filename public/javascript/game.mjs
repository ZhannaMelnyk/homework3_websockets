import {
  updateRoomsList,
  updatePlayersInGameRoom,
  updatePlayerInfo,
  updateGameView,
  updateTimerBeforeGame,
  updateTimerForGame,
  showTextInGameContainer,
  updatePlayersProgressIndicator, 
  showGameLeaderBoard
} from './helpers/updateHelper.mjs';
import { toGameRoom, toLobby } from './helpers/changeRoomHelper.mjs';
import { getTextByIndex } from './helpers/apiHelper.mjs';

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("http://localhost:3002/game", { query: { username } });

const createNewRoomBtn = document.getElementById('create-room-btn');

createNewRoomBtn.addEventListener('click', () => {
  const newRoomName = prompt('Input Room Name');
  if (newRoomName) {
    socket.emit('CREATE_NEW_ROOM', newRoomName);
  }
})

const updateRooms = rooms => updateRoomsList(rooms, socket);

const goToGameRoom = room => toGameRoom(room, socket);

const updateRoom = players => updatePlayersInGameRoom(players, socket);

const connectUserToNewlyCreatedRoom = roomId => {
  socket.emit('JOIN_ROOM', roomId);
}

const askToChangeRoomName = roomName => {
  const newRoomName = prompt(`Room "${roomName}" already exists.
Please enter new name:`);
  if (newRoomName) {
    socket.emit('CREATE_NEW_ROOM', newRoomName);
  }
}

const goToLobby = rooms => toLobby(rooms, socket);

const updatePlayer = room => updatePlayerInfo(room, socket);

const updateGame = roomId => updateGameView(roomId, socket);

const updateBeforeTimer = timeBeforeStart => updateTimerBeforeGame(timeBeforeStart);

const updateGameTimer = gameTime => updateTimerForGame(gameTime);

const getText = (roomId, textIndex) => getTextByIndex(roomId, textIndex, socket);

const showText = (textArray, player, roomId) => showTextInGameContainer(textArray, player, roomId, socket);

const updateProgressIndicator = (textArray, player) => updatePlayersProgressIndicator(textArray, player);

const showLeaderBoard = (leadersList, roomId) => showGameLeaderBoard(leadersList, roomId, socket);

socket.on('UPDATE_ROOMS', updateRooms);
socket.on('JOIN_ROOM_DONE', goToGameRoom);
socket.on('UPDATE_ROOM', updateRoom);
socket.on('CREATE_NEW_ROOM_DONE', connectUserToNewlyCreatedRoom);
socket.on('CREATE_NEW_ROOM_NOT_DONE', askToChangeRoomName)
socket.on('LEAVE_ROOM_DONE', goToLobby);
socket.on('UPDATE_USER_INFO', updatePlayer);
socket.on('REMOVE_ISREADY_BUTTON', updateGame);
socket.on('CHANGE_TIMER_BEFORE_GAME', updateBeforeTimer);
socket.on('CHANGE_GAME_TIMER', updateGameTimer);
socket.on('GET_TEXT_INDEX', getText);
socket.on('SHOW_GAME_TEXT', showText);
socket.on('CHANGE_PROGRESS_INDICATOR', updateProgressIndicator)
socket.on('TIME_END', showLeaderBoard)