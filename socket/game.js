import * as config from "./config";

const roomsMap = new Map();
const getCurrentRoomsList = () => {
  let currentRooms = [];
  roomsMap.forEach(room => {
    if (room.players.length > 0
      && room.players.length < config.MAXIMUM_USERS_FOR_ONE_ROOM
      && !room.isTimerStart) {
      currentRooms.push(room)
    }
  });
  return currentRooms;
}

const isAllPlayersReady = players => {
  for (let i = 0; i < players.length; i++) {
    if (!players[i].isReady) {
      return false
    }
  }
  return true
}

let winnerTablePosition = 0;

const getLeaderBoard = players => {
  players = players.map(player => {
    if (player.winnerTablePosition == 0) {
      player.winnerTablePosition = 6;
    }
    return player;
  })
  let leaderBoard = players.sort(compare);
  return leaderBoard;
}

const compare = (a, b) => {
  const playerOne = a.winnerTablePosition;
  const playerTwo = b.winnerTablePosition;

  let comparison = 0;
  if (playerOne > playerTwo) {
    comparison = 1;
  } else if (playerOne < playerTwo) {
    comparison = -1;
  }
  return comparison;
}

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const currentRoomsList = getCurrentRoomsList();

    let currentRoom;
    socket.emit('UPDATE_ROOMS', currentRoomsList);

    socket.on('CREATE_NEW_ROOM', name => {
      if (!roomsMap.has(name)) {
        roomsMap.set(name, {
          name,
          text: [],
          isTimerStart: false,
          isFinished: false,
          players: [],
          textIndex: -1
        })

        const currentRoomsList = getCurrentRoomsList();
        socket.broadcast.emit('UPDATE_ROOMS', currentRoomsList);
        socket.emit('CREATE_NEW_ROOM_DONE', name);
      } else {
        socket.emit('CREATE_NEW_ROOM_NOT_DONE', name);
      }
    })

    socket.on('JOIN_ROOM', roomId => {
      currentRoom = roomsMap.get(roomId);

      currentRoom.players.push({
        socketID: socket.id,
        name: username,
        isReady: false,
        currentWordIndex: 0,
        winnerTablePosition
      });
      roomsMap.set(roomId, currentRoom);

      socket.join(roomId, () => {
        io.to(socket.id).emit('JOIN_ROOM_DONE', currentRoom);
      });
      socket.to(roomId).emit("UPDATE_ROOM", currentRoom.players);

      const currentRoomsList = getCurrentRoomsList();
      socket.broadcast.emit('UPDATE_ROOMS', currentRoomsList);
    })

    socket.on('LEAVE_ROOM', roomId => {
      const currentRoom = roomsMap.get(roomId);

      const deleteUser = currentRoom.players.map(player => {
        if (player.name === username) {
          player = undefined;
        }
        return player;
      })
      currentRoom.players = deleteUser.filter(player => player != undefined);

      roomsMap.set(roomId, currentRoom);

      let currentRoomsList;

      socket.leave(roomId, () => {
        currentRoomsList = getCurrentRoomsList();
        io.to(socket.id).emit('LEAVE_ROOM_DONE', currentRoomsList);

        if (currentRoom.players.length > 0) {
          io.to(roomId).emit("UPDATE_ROOM", currentRoom.players);
          if (currentRoom.players.length > 1 && isAllPlayersReady(currentRoom.players)) {
            currentRoom.isTimerStart = true;
            roomsMap.set(roomId, currentRoom);
            io.to(roomId).emit("REMOVE_ISREADY_BUTTON", roomId);
          }
        } else {
          roomsMap.delete(roomId);
          currentRoomsList = getCurrentRoomsList();
        }
        io.emit('UPDATE_ROOMS', currentRoomsList);
      });
    })

    socket.on('PLAYER_IS_READY', (roomId, username) => {
      const currentRoom = roomsMap.get(roomId);
      currentRoom.players = currentRoom.players.map(player => {
        if (player.name === username) {
          player.isReady = true;
        }
        return player;
      })
      roomsMap.set(roomId, currentRoom);

      socket.emit("UPDATE_USER_INFO", currentRoom);
      socket.to(roomId).emit("UPDATE_ROOM", currentRoom.players);

      if (currentRoom.players.length > 1 && isAllPlayersReady(currentRoom.players)) {
        currentRoom.isTimerStart = true;
        roomsMap.set(roomId, currentRoom);
        io.to(roomId).emit("REMOVE_ISREADY_BUTTON", roomId);
      }
    })

    socket.on('PLAYER_IS_NOT_READY', (roomId, username) => {
      const currentRoom = roomsMap.get(roomId);
      currentRoom.players = currentRoom.players.map(player => {
        if (player.name === username) {
          player.isReady = false;
        }
        return player;
      })

      roomsMap.set(roomId, currentRoom);
      socket.emit("UPDATE_USER_INFO", currentRoom);
      socket.to(roomId).emit("UPDATE_ROOM", currentRoom.players);
    })

    socket.on('START_TIMERS', roomId => {
      let currentRoom = roomsMap.get(roomId);

      if (currentRoom.textIndex == -1) {
        currentRoom.textIndex = Math.round(Math.random() * 6)
        socket.emit('GET_TEXT_INDEX', roomId, currentRoom.textIndex);
        roomsMap.set(roomId, currentRoom);
      }

      let secondsBeforeStart = config.SECONDS_TIMER_BEFORE_START_GAME;

      io.emit('UPDATE_ROOMS', currentRoomsList);

      const changeBeforeGameTimer = setInterval(() => {
        if (secondsBeforeStart > 0) {
          socket.to(roomId).emit('CHANGE_TIMER_BEFORE_GAME', secondsBeforeStart);
          secondsBeforeStart--;
        } else {
          clearInterval(changeBeforeGameTimer);

          let secondsForGame = config.SECONDS_FOR_GAME;
          currentRoom = roomsMap.get(roomId);
          const currentPlayer = currentRoom.players.find(player => player.name === username);

          socket.to(roomId).emit('SHOW_GAME_TEXT', currentRoom.text.split(''), currentPlayer, roomId);
          const changeGameTimer = setInterval(() => {
            if (secondsForGame >= 0) {
              socket.to(roomId).emit('CHANGE_GAME_TIMER', secondsForGame);
              secondsForGame--;
            } else {
              currentRoom.isFinished = true;

              roomsMap.set(roomId, currentRoom);

              clearInterval(changeGameTimer);

              const leaderBoard = getLeaderBoard(currentRoom.players);
              socket.to(roomId).emit('TIME_END', leaderBoard, roomId);
            }
          }, 1000)
        }
      }, 1000)
    })

    socket.on('GET_TEXT_BY_ID_DONE', (roomId, text) => {
      let currentRoom = roomsMap.get(roomId);
      currentRoom.text = text;
      roomsMap.set(roomId, currentRoom);
    })

    socket.on('INPUT_LETTER', (inputKey, roomId) => {
      let currentRoom = roomsMap.get(roomId);
      const currentPlayer = currentRoom.players.find(player => player.name === username);
      const textArray = currentRoom.text.split('')

      if (inputKey === textArray[currentPlayer.currentWordIndex]) {
        currentRoom.players = currentRoom.players.map(player => {
          if (player.name === username) {
            player.currentWordIndex++;
          }
          return player;
        })
        roomsMap.set(roomId, currentRoom);
        if (textArray[currentPlayer.currentWordIndex] === undefined) {
          currentRoom.players = currentRoom.players.map(player => {
            if (player.name === username) {
              player.currentWordIndex++;
              player.winnerTablePosition = ++winnerTablePosition;
            }
            return player;
          })
          roomsMap.set(roomId, currentRoom);
        }
        const updatedCurrentPlayer = currentRoom.players.find(player => player.name === username);
        io.to(socket.id).emit('SHOW_GAME_TEXT', currentRoom.text.split(''), updatedCurrentPlayer, roomId);
        socket.to(roomId).emit('CHANGE_PROGRESS_INDICATOR', currentRoom.text.split(''), updatedCurrentPlayer, roomId);
        socket.emit('CHANGE_PROGRESS_INDICATOR', currentRoom.text.split(''), updatedCurrentPlayer, roomId);
      } else {
        io.to(socket.id).emit('SHOW_GAME_TEXT', currentRoom.text.split(''), currentPlayer, roomId);
      }
    })

    socket.on('disconnect', () => {
      if (currentRoom) {
        const deleteUser = currentRoom.players.map(player => {
          if (player.name === username) {
            player = undefined;
          }
          return player;
        })
        currentRoom.players = deleteUser.filter(player => player != undefined);

        roomsMap.set(currentRoom.name, currentRoom);

        socket.leave(currentRoom.name, () => {
          const currentRoomsList = getCurrentRoomsList();
          socket.broadcast.emit('UPDATE_ROOMS', currentRoomsList);
        });
        io.to(currentRoom.name).emit("UPDATE_ROOM", currentRoom.players);
      }
    });
  });
};
