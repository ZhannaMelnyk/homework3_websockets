const usersSet = new Set();

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;

    socket.on('CHECK_USERNAME', username => {
      if (usersSet.has(username)) {
        socket.emit('NEED_RELOGIN', username);
      } else {
        usersSet.add(username);
      }
    });
  });
};